package org.go.hansol.service;

import lombok.extern.log4j.Log4j2;
import org.go.hansol.domain.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Log4j2
@SpringBootTest
public class RNRServiceTests {

    @Autowired
    private RNRService service;

    @Test
    public void registerTest() {

        RNRVO vo = RNRVO.builder()
                .kind("test service...")
                .eno(1)
                .tno("W-00001")
                .cnoList(List.of(new Integer[]{1, 2}))
                .build();

        service.register(vo);

    }

    @Test
    public void readTest() {
        service.read().forEach(i -> log.info(i));
    }

    @Test
    public void readOneTest() {
        log.info(service.readOne(3));
    }

    @Test
    public void removeTest() {
        service.remove(60);
    }

    @Test
    public void modifyTest() {
        RNRVO vo = RNRVO.builder()
                .rno(4)
                .kind("test service modify...")
                .eno(1)
                .tno("W-00004")
                .cnoList(List.of(new Integer[]{2}))
                .build();

        service.modify(vo);
    }
}
