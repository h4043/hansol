package org.go.hansol.mapper;

import lombok.extern.log4j.Log4j2;
import org.go.hansol.domain.RNRVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Log4j2
@SpringBootTest
public class RNRMapperTests {

    @Autowired
    private RNRMapper mapper;

    @Test
    public void selectAllRNRTest() {
        mapper.selectAll().forEach(i -> log.info(i));
    }

    @Test
    public void selectOneTest() {
        log.info(mapper.selectOne(3));
    }

    @Test
    public void deleteOneTest() {
        log.info(mapper.delete(61));
    }

    @Test
    public void deleteCompanyTest() {
        mapper.deleteRNRCompany(58);
    }

    @Test
    public void updateTest() {
        RNRVO vo = RNRVO.builder()
                .rno(1)
                .kind("test mapper update omg........")
                .eno(7)
                .tno("W-00007")
                .build();
        mapper.update(vo);
    }

    @Test
    public void updateCompanyTest() {
        RNRVO vo = RNRVO.builder()
                .rno(4)
                .kind("test mapper update omg........")
                .eno(7)
                .tno("W-00007")
                .cnoList(List.of(new Integer[]{1}))
                .build();
        mapper.updateRNRCompany(4, 1);
    }

    @Test
//    @Transactional
    public void insertTest() {

        RNRVO vo = RNRVO.builder()
                .kind("test mapper t...")
                .eno(1)
                .tno("W-00001")
                .cnoList(List.of(new Integer[]{1, 2}))
                .build();

        mapper.insertRNR(vo);
        mapper.insertRNRCompany(vo);
    }
}
