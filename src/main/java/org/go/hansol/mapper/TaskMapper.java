package org.go.hansol.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.go.hansol.domain.TaskVO;

import java.util.List;

@Mapper
public interface TaskMapper {

    List<TaskVO> selectAll();
    TaskVO selectOne();
    int update(String tno);
    int delete(String tno);
}
