package org.go.hansol.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.go.hansol.domain.EmployeeVO;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    List<EmployeeVO> selectAll();
    EmployeeVO selectOne();
    int update(Integer eno);
    int delete(Integer eno);
}
