package org.go.hansol.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.go.hansol.domain.RNRDTO;
import org.go.hansol.domain.RNRVO;

import java.util.List;

@Mapper
public interface RNRMapper {

    List<RNRDTO> selectAll();
    RNRDTO selectOne(Integer rno);

    int insertRNR(RNRVO vo);
    int insertRNRCompany(RNRVO vo);

    int update(RNRVO vo);
    int updateRNRCompany(@Param("rno")Integer rno, @Param("cno")Integer cno);

    int delete(Integer rno);
    int deleteRNRCompany(Integer rno);
}
