package org.go.hansol.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.go.hansol.domain.CompanyVO;

import java.util.List;

@Mapper
public interface CompanyMapper {

    List<CompanyVO> selectAll();
    CompanyVO selectOne();
    int update(Integer cno);
    int delete(Integer cno);
}
