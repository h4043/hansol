package org.go.hansol.service;

import org.go.hansol.domain.*;

import java.util.List;

public interface RNRService {
    List<RNRDTO> read();
    RNRDTO readOne(Integer rno);
    void register(RNRVO vo);
    void modify(RNRVO vo);
    void remove(Integer rno);

    List<TaskVO> taskRead();
    List<CompanyVO> companyListRead();
    List<EmployeeVO> employeeListRead();
}
