package org.go.hansol.service;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.go.hansol.domain.*;
import org.go.hansol.mapper.CompanyMapper;
import org.go.hansol.mapper.EmployeeMapper;
import org.go.hansol.mapper.RNRMapper;
import org.go.hansol.mapper.TaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
public class RNRServiceImpl implements RNRService {

    @Setter(onMethod_ = {@Autowired})
    private RNRMapper rnrMapper;

    @Setter(onMethod_ = {@Autowired})
    private TaskMapper taskMapper;

    @Setter(onMethod_ = {@Autowired})
    private CompanyMapper companyMapper;

    @Setter(onMethod_ = {@Autowired})
    private EmployeeMapper employeeMapper;


    @Override
    public List<RNRDTO> read() {
        return rnrMapper.selectAll();
    }

    @Override
    public RNRDTO readOne(Integer rno) {
        return rnrMapper.selectOne(rno);
    }

//    @Transactional
    @Override
    public void register(RNRVO vo) {
        rnrMapper.insertRNR(vo);
        rnrMapper.insertRNRCompany(vo);
    }

    @Override
    public void modify(RNRVO vo) {
        // 기존 회사 데이터 지우고 다시 등록
        if (vo.getCnoList() != null) {
            rnrMapper.deleteRNRCompany(vo.getRno());
            vo.getCnoList().forEach(cno -> rnrMapper.updateRNRCompany(vo.getRno(), cno));
        }
        rnrMapper.update(vo);
    }

    @Override
    public void remove(Integer rno) {
        rnrMapper.delete(rno);
    }

    @Override
    public List<TaskVO> taskRead() {
        return taskMapper.selectAll();
    }

    @Override
    public List<CompanyVO> companyListRead() {
        return companyMapper.selectAll();
    }

    @Override
    public List<EmployeeVO> employeeListRead() {
        return employeeMapper.selectAll();
    }
}
