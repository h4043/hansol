package org.go.hansol.controller;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.go.hansol.domain.*;
import org.go.hansol.service.RNRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@CrossOrigin
@RequestMapping("/rnr")
@RestController
public class RNRController {

    @Setter(onMethod_ = {@Autowired})
    private RNRService rnrService;

    @PostMapping(value="/register", consumes = "application/json", produces = { MediaType.TEXT_PLAIN_VALUE })
    public void register(@RequestBody RNRVO vo) {   // 테스트 메서드
        log.info(vo);
        rnrService.register(vo);
    }

    @ResponseBody
    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RNRDTO>> read() {
        return new ResponseEntity<>(rnrService.read(), HttpStatus.OK);
    }

    @GetMapping(value = "/{rno}")
    public RNRDTO readOne(@PathVariable("rno") Integer rno) {
        return rnrService.readOne(rno);
    }

    @PostMapping(value = "/remove")
    public void remove(Integer rno) {
        rnrService.remove(rno);
    }

    @PostMapping(value = "/modify")
    public void modify(RNRVO vo) {
        rnrService.modify(vo);
    }

    @ResponseBody
    @GetMapping(value = "/task/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TaskVO>> taskRead() {
        return new ResponseEntity<>(rnrService.taskRead(), HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(value = "/company/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CompanyVO>> companyListRead() {
        return new ResponseEntity<>(rnrService.companyListRead(), HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(value = "/employee/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmployeeVO>> employeeListRead() {
        return new ResponseEntity<>(rnrService.employeeListRead(), HttpStatus.OK);
    }
}


