package org.go.hansol.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class CompanyVO {

    private Integer cno;
    private String cname;
}
