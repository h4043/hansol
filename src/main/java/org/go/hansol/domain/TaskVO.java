package org.go.hansol.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class TaskVO {

    private String tno;
    private String tname;
}
