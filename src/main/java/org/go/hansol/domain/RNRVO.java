package org.go.hansol.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
@ToString
public class RNRVO {

    // tbl_rnr
    private Integer rno;
    private String kind;
    private String tno;
    private Integer eno;

    // tbl_rnr_company
    private List<Integer> cnoList;
}
