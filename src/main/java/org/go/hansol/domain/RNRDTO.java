package org.go.hansol.domain;

import lombok.*;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RNRDTO {

    private Integer rno;
    private String kind;

    private TaskVO task;
    private EmployeeVO employee;

    private List<CompanyVO> companyList;
}
