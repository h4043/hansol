package org.go.hansol.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class EmployeeVO {

    private Integer eno;
    private String name;
    private String position;
    private String tel;
}
